package com.nitesh.mysqlpoc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer bid;

	private String title;

	private String author;

	private String publication;

	private float price;

	private String country;
	
	private Integer page;

	public Book() {}


	public Book(Integer bid, String title, String author, String publication, float price, String country,
			Integer page) {
		super();
		this.bid = bid;
		this.title = title;
		this.author = author;
		this.publication = publication;
		this.price = price;
		this.country = country;
		this.page = page;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getBid() {
		return bid;
	}


	public void setBid(Integer bid) {
		this.bid = bid;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getPublication() {
		return publication;
	}


	public void setPublication(String publication) {
		this.publication = publication;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public Integer getPage() {
		return page;
	}


	public void setPage(Integer page) {
		this.page = page;
	}


	@Override
	public String toString() {
		return "Book [id=" + id + ", bid=" + bid + ", title=" + title + ", author=" + author + ", publication="
				+ publication + ", price=" + price + ", country=" + country + ", page=" + page + "]";
	}
	
}


/*CREATE TABLE `book` (
-> `id` INT(11) NOT NULL AUTO_INCREMENT,
-> `bid` INT(11) DEFAULT NULL,
-> `title` VARCHAR(50) DEFAULT NULL,
-> `author` VARCHAR(50) DEFAULT NULL,
-> `publication` VARCHAR(50) DEFAULT NULL,
-> `price` FLOAT DEFAULT NULL,
-> `country` VARCHAR(50) DEFAULT NULL,
-> `page` INT(11) DEFAULT NULL,
-> UNIQUE KEY `bid_index` (`bid`) USING BTREE,
-> KEY `title_index` (`title`) USING BTREE,
-> KEY `author_index` (`author`) USING BTREE,
-> KEY `page_index` (`page`) USING BTREE,
-> PRIMARY KEY (`id`)
-> ) ENGINE=InnoDB;*/