package com.nitesh.mysqlpoc.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.nitesh.mysqlpoc.model.Book;

public interface BookRepository extends CrudRepository<Book, Integer> {

	@Transactional
	@Query(value="UPDATE book set author=:author where id=:id", nativeQuery = true)
	public void updateAuthorById(@Param("id") int id,@Param("author") String author);
}
