package com.nitesh.mysqlpoc.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RecordLoader {


  private static final String INSERT_SQL = "INSERT INTO book (bid, author, country, price, publication, title) VALUES (?, ?, ? ,?, ?,?)";

  public static void main(String[] args) {
    String jdbcUrl = "jdbc:mysql://localhost:3306/testdb";
    String username = "root";
    String password = "root";

    Connection conn = null;
    PreparedStatement stmt = null;

    try {
      // Open connection
      conn = DriverManager.getConnection(jdbcUrl, username, password);

      // Create statement
      stmt = conn.prepareStatement(INSERT_SQL);
      
      stmt.setInt(1, 101);
      stmt.setString(2, "Nitesh Nandan");
      stmt.setString(3, "Bharat");
      stmt.setFloat(4, 100);
      stmt.setString(5, "HindustanTimes");
      stmt.setString(6, "Jiho");
      stmt.executeUpdate();
      System.out.println("Records inserted");

    } catch (SQLException  e) {
      e.printStackTrace();
    } finally {
      try {
        // Close connection
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
